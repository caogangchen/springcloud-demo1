package com.cg.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class, args);
    }
    /**
     * Eureka-Server源码分析：
     * @EnableEurekaServer
     * EurekaServerMarkerConfiguration
     * return new Marker();
     *
     *  EurekaServerAutoConfiguration
     *  --EurekaServerConfigBeanConfiguration
     *  初始化EurekaServerConfigBean
     *
     *  EurekaServerBootstrap
     *
     *  EurekaServerInitializerConfiguration
     *   ----SmartLifecycle Spring生命周期类 如果isAutoStartup是true会自动启动
     *   ----此处用法就是当Spring所有Bean加载完成后启动Eureka服务器。实现服务注册与发现。
     *   EurekaServerInitializerConfiguration.start()
     *   eurekaServerBootstrap.contextInitialized(EurekaServerInitializerConfiguration.this.servletContext);
     *   ----initEurekaEnvironment(); 初始化Eureka运行环境
     *   ----initEurekaServerContext();初始化Eureka上下文
     *   int registryCount = this.registry.syncUp(); 服务同步
     *   this.registry.openForTraffic(this.applicationInfoManager, registryCount); 服务剔除
     *
     *   服务端http接口
     *   ApplicationsResource
     *   getContainers()
     *   Key cacheKey = new Key(Key.EntityType.Application,
     *                 ResponseCacheImpl.ALL_APPS,
     *                 keyType, CurrentRequestVersion.get(), EurekaAccept.fromString(eurekaAccept), regions
     *         );
     *
     *  response = Response.ok(responseCache.get(cacheKey)).build();
     *  responseCache.get(cacheKey)
     *  ***重要
     *  return get(key, shouldUseReadOnlyResponseCache);
     *  Value payload = getValue(key, useReadOnlyCache);
     *  final Value currentPayload = readOnlyCacheMap.get(key);  //从只读缓存去拿
     *  payload = readWriteCacheMap.get(key);  //从读写缓存去拿
     *  readWriteCacheMap如何生成的
     *  ResponseCacheImpl(EurekaServerConfig serverConfig, ServerCodecs serverCodecs, AbstractInstanceRegistry registry)
     *  Value value = generatePayload(key);
     *  payload = getPayLoad(key, registry.getApplications());
     *  registry.getApplications()      //读写缓存中的回调方法 从真正的内存中去拿数据
     *  registry:eureka真正的注册表数据
     *
     *
     *
     *  ApplicationResource 注册接口 addInstance
     *  registry.register(info, "true".equals(isReplication));
     *  PeerAwareInstanceRegistryImpl ：registry.register(info, "true".equals(isReplication));
     *  AbstractInstanceRegistry：register()
     *  清理缓存
     *  invalidateCache(registrant.getAppName(), registrant.getVIPAddress(), registrant.getSecureVipAddress());
     *
     */


    /***
     * Eureka-Client源码分析:
     * EurekaClientAutoConfiguration
     * EurekaClientConfiguration
     * EurekaDiscoveryClientConfiguration
     * new CloudEurekaClient
     * super(applicationInfoManager, config, args);
     * DiscoveryClient 构造方法
     *
     *
     * return new EurekaDiscoveryClient(client, clientConfig);
     *
     * // finally, init the schedule tasks (e.g. cluster resolvers, heartbeat, instanceInfo replicator, fetch
     * initScheduledTasks(); 重要
     * new TimedSupervisorTask
     * TimedSupervisorTask 定时任务闪光点
     *
     * new CacheRefreshThread() 获取服务端注册表信息
     *  ---refreshRegistry();
     *  拉取注册表
     *  --boolean success = fetchRegistry(remoteRegionsModified);
     *
     *
     * 首次全量更新： getAndStoreFullRegistry();  增量更新：getAndUpdateDelta(applications);
     * 获取服务端的所有服务
     * eurekaTransport.queryClient.getApplications   使用AbstractJerseyEurekaHttpClient调用 类似Springmvc
     * return getApplicationsInternal("apps/", regions);
     * 放入缓存中
     * localRegionApps.set(this.filterAndShuffle(apps));
     *
     * fetchRegistry()
     * getAndUpdateDelta() 计算hashcode 判断和服务端缓存是否一致 不一致重新拉取
     *
     *
     * // finally, init the schedule tasks (e.g. cluster resolvers, heartbeat, instanceInfo replicator, fetch
     * initScheduledTasks(); 重要
     * 初始化注册任务
     * instanceInfoReplicator.start(clientConfig.getInitialInstanceInfoReplicationIntervalSeconds());
     *
     */

    /**
     * eureka三级缓存原理：保证AP 高可用 分区容错 无法保证C强一致性 保证了最终一致性
     * 在拉取注册表的时候：
     * 首先从ReadOnlyCacheMap里查缓存的注册表
     * 若没有，就找ReadWriteCacheMap里缓存的注册表。
     * 如果还没有，就从内存中获取实际的注册表数据
     *
     * 在注册表发生变更的时候：
     * 会在内存中更新变更的注册表数据，同时过期掉ReadWriteCacheMap
     * 此过程不会影响ReadOnlyCacheMap提供服务查询注册表
     * 默认每30秒Eureka Server会将ReadWriteCacheMap更新到ReadOnlyCacheMap里
     * 默认每180秒Eureka Server会将ReadWriteCacheMap里数据失效
     * 下次有服务拉取注册表，又会从内存中获取更新的数据了，同时填充各级缓存
     */

    /**
     * 诡异问题：
     * 多级缓存源码：
     * 当我们Eureka服务实例有注册或下线或有实例发生故障，内存注册表虽然会及时更新数据，
     * 但是客户端不一定能及时感知到，可能会过30秒才能感知到，因为客户端拉取注册表实例这
     * 里边有一个多级缓存机制。
     * 服务剔除的不是默认90秒没心跳的实例，剔除的是180秒没心跳的实例(eureka bug)
     */

}
