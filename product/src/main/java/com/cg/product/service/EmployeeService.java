package com.cg.product.service;

import com.cg.product.entity.Employee;
import com.cg.product.support.service.IBaseService;

/**
 * @Description:
 * @Date: 2022-04-19 16:58
 * @Author: cao_gang
 */
public interface EmployeeService extends IBaseService<Employee> {
    void test();
}
