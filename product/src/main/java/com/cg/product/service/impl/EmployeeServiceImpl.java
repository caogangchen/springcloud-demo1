package com.cg.product.service.impl;

import com.cg.product.entity.Employee;
import com.cg.product.mapper.EmployeeMapper;
import com.cg.product.service.EmployeeService;
import com.cg.product.support.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description:
 * @Date: 2022-04-19 16:58
 * @Author: cao_gang
 */
@Service
public class EmployeeServiceImpl extends BaseServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public void test() {
        Employee employee=new Employee();
        employee.setName("Renogy");
        this.save(employee);
//        System.out.println(10/0);
    }
}
