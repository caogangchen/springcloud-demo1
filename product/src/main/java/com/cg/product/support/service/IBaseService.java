package com.cg.product.support.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @InterfaceName IBaseService
 * @Description: 公共服务接口
 * @Author: cao_gang
 * @Date: 2020/6/6
 **/
public interface IBaseService<T> extends IService<T> {
}
