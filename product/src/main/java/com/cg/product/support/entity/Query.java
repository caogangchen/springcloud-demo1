package com.cg.product.support.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @ClassName: Query
 * @Description: 分页查询的条件
 * @Author: cao_gang
 * @Date: 2020-06-10 19:42
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "查询条件")
public class Query {
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页",example = "1", required = true)
    @NotNull(message = "current {Validation.NotNull}")
    private Integer current;

    /**
     * 每页的数量
     */
    @ApiModelProperty(value = "每页的数量",example = "10", required = true)
    @NotNull(message = "size {Validation.NotNull}")
    private Integer size;

    /**
     * 排序的字段名
     */
    @ApiModelProperty(hidden = true)
    private String ascs;

    /**
     * 排序方式
     */
    @ApiModelProperty(hidden = true)
    private String descs;
}
