package com.cg.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("employee")
public class Employee {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", lastName='" + name + '\'' +
                '}';
    }
}
