package com.cg.order.service;

import com.cg.order.entity.User;
import com.cg.order.support.service.IBaseService;

/**
 * @Description:
 * @Date: 2022-04-19 16:56
 * @Author: cao_gang
 */
public interface UserService extends IBaseService<User> {

    void test();
}
