package com.cg.order.service.impl;

import com.cg.order.entity.User;
import com.cg.order.mapper.UserMapper;
import com.cg.order.service.ProductService;
import com.cg.order.service.UserService;
import com.cg.order.support.service.impl.BaseServiceImpl;
//import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description:
 * @Date: 2022-04-19 16:56
 * @Author: cao_gang
 */
@Service
@Slf4j
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements UserService {

    private final ProductService productService;

    public UserServiceImpl(ProductService productService) {
        this.productService = productService;
    }

//    @GlobalTransactional
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public void test() {
        productService.test();
        User user=new User();
        user.setAge(123);
        user.setEmail("test@123.com");
        user.setName("test");
        this.save(user);
        System.out.println(10/0);
        log.info("success");

    }
}
