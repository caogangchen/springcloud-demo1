package com.cg.order.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Description: 接口描述
 * @Author: cao_gang
 * @Date: 2021-09-13 13:36
 */
@FeignClient(value = "product-service")
public interface ProductService {
    @GetMapping("/employee/test")
    String test();
}
