package com.cg.order.support.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cg.order.support.service.IBaseService;

/**
 * @ClassName BaseServiceImpl
 * @Description: 公共服务实现类
 * @Author: cao_gang
 * @Date: 2020/6/6 16:55:53
 **/
public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements IBaseService<T> {
}
