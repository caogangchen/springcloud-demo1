package com.cg.order.controller;

import com.cg.order.service.ProductService;
import com.cg.order.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 类描述
 * @Author: cao_gang
 * @Date: 2021-09-13 13:33
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test")
    public String test() {
        userService.test();
        return "测试";
    }
}
