package com.cg.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cg.order.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper extends BaseMapper<User> {
}
